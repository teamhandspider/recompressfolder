﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace recompressfolder
{
    class Program
    {
        private static int totalVidCount;
        private static int processingVideoIndex;

        public static string MainPath {
            get {
                var exepath = Process.GetCurrentProcess().MainModule.FileName;

                var iteratingpath = new FileInfo(exepath).Directory;
                while (true) {
                    if (iteratingpath.Name == "recompressfolder")
                        return iteratingpath.FullName;

                    iteratingpath = iteratingpath.Parent;
                }
            }
        }

        public static string FFMPEGPath {
            get {
                return Path.Combine(MainPath, "ffmpeg.exe");
            }
        }


        static void Main(string[] args) {

            args = new[] { @"E:\atttttt\ORIGINALS" };            

            var folder = new DirectoryInfo(args[0]);

            totalVidCount = RecompressFolderRecursive(folder, folder, true);
            Console.WriteLine("Going to recompress " + totalVidCount + " files");
            Thread.Sleep(3000);

            RecompressFolderRecursive(folder, folder);
        }

        private static int RecompressFolderRecursive(DirectoryInfo folder, DirectoryInfo rootFolder, bool dryRun = false) {

            int cnt = 0;

            foreach (var item in folder.GetFiles("*.mov")) {
                if (!dryRun) Recompress(item, rootFolder);
                if(!dryRun)processingVideoIndex++;
                cnt++;
                //return 0; //after one
            }

            foreach (var item in folder.GetDirectories()) {
                cnt += RecompressFolderRecursive(item, rootFolder, dryRun);
            }

            return cnt;
        }

        private static void Recompress(FileInfo item, DirectoryInfo rootFolder) {
            var pth = item.FullName;

            //var outPath = @"C:\temp\attest.mov";
            var relativePath = item.FullName.Replace(rootFolder.FullName,"");
            var rootNewFolder = rootFolder.FullName + "_recompressed";
            //var outPath = Path.Combine(rootNewFolder, relativePath);
            var outPath = rootNewFolder + "" +  relativePath;
            new FileInfo(outPath).Directory.Create();

            var arg = $"-i \"{pth}\" -crf 13 -pix_fmt yuv420p {outPath}";

            ProcessRunningUtils.RunExe(FFMPEGPath, new[] { arg }, out string ingore, out string ignore2, true, onErrorDataReceived: OnFFmpeg);
        }

        private static void OnFFmpeg(string obj) {
            if (obj.Contains("frame= ")) {
                var indx = obj.IndexOf("frame= ") + "frame= ".Length;

                string numBuf = "";
                int numscanner = indx;
                while (true) {
                    if (obj[numscanner] == ' ') {
                        //do nothing
                    }
                    else if (int.TryParse(obj[numscanner].ToString(), out int ignore)) {
                        numBuf = numBuf + obj[numscanner];
                    }
                    else break;

                    numscanner++;
                }
                //Debug.Log("FRAME from debugloglevel:" + numBuf);
                var lastObtainedFrame = float.Parse(numBuf);

                Console.WriteLine($"PROGRESS: video:{processingVideoIndex+1} / {totalVidCount} frame:{lastObtainedFrame}");
            }
            Console.WriteLine(obj);
        }
    }
}
